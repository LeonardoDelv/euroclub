<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Homepage - EuroClub</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    sec_session_start();
    //$operatori = $dbh->getOperatori();
    //$domicilieri = $dbh->getDomicilieri();

    $baristi = $dbh->getBaristi();
    $clienti = $dbh->getClienti();
    //$domicilieri = $dbh->getDomicilieri();
    require_once 'modals.php';
    ?>
    <style media="screen">
      button {
        margin-top: 4%;
      }
    </style>

    <div class="container justify-content-center col-md-6">
      <hr class="upRegister">
      <h4 class="text-center">Login Titolare</h4>
      <div class="container justify-content-center col-md-6">
        <div class="form-group">
          <form id="adminnn" action="home_admin.php" method="post">
          <label for="admins">Seleziona Identificativo</label>
            <select name="admins"  class="form-control">
              <option value="1"> 1 </option>
            </select>
          <div class="container justify-content-center col-md-6">
            <button class="btn btn-primary" type="submit">Login</button>
          </div>
          </form>
        </div>
      </div>
      <hr class="upRegister">
      <h4 class="text-center">Login Barista</h4>
      <div class="form-group">
        <form id="barista" action="home_barista.php" method="post">
          <div class="container justify-content-center col-md-6">
            <label for="barista">Codice Fiscale</label>
            <select name="barista"  class="form-control">
              <?php foreach($baristi as $barista): ?>
              <option value="<?php echo $barista['codFiscale'];?>"><?php echo $barista['codFiscale'];?></option>
              <?php endforeach; ?>
            </select>
            <div class="container justify-content-center col-md-6">
              <button class="btn btn-primary" type="submit">Login</button>
            </div>
          </div>
        </form>
      </div>
      <hr class="upRegister">
      <h4 class="text-center">Login Cliente</h4>
      <div class="form-group">
        <form id="cliente" action="home_cliente.php" method="post">
          <div class="container justify-content-center col-md-6">
            <label for="cliente">Codice Fiscale</label>
            <select name="cliente" class="form-control">
              <?php foreach($clienti as $cliente): ?>
              <option value="<?php echo $cliente['codFiscale']; ?>"><?php echo $cliente['codFiscale']; ?></option>
              <?php  endforeach;  ?>
            </select>
            <div class="container justify-content-center col-md-6">
              <button class="btn btn-primary" type="submit">Login</button>
            </div>
          </div>
        </form>
      </div>
  </body>
</html>
