<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Barista</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php';?>
    <?php sec_session_start();
    if(!empty($_POST['operatore'])){
      $_SESSION['operatore'] = $_POST['operatore'];
    }
    ?>
    <?php require_once 'navbar_home.php';
    ?>

    <div class="container justify-content-center col-md-9">
      <hr>
        <h3 class="text-center">Home Operatore</h3>
      <hr>
      <h5 class="text-center">Inserisci un nuovo cliente</h5>
      <br>
      <div class="form-group">
        <form id="iD" action="insert_cliente.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-danger">Inserisci Cliente</button>
        </div>
      </div>

      <hr>
      <h5 class="text-center">Inserisci nuove prenotazioni</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_ordine.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php echo $_POST['operatore']; ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Prenotazione</button>
        </div>
      </div>


      <hr>
      <h5 class="text-center">Inserisci turni operatore</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_turno_op.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php echo $_POST['operatore']; ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-danger">Inserisci turno operatore</button>
        </div>
      </div>


      <hr>
      <h5 class="text-center">Inserisci turni domiciliere</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_turno_dom.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php echo $_POST['operatore']; ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci turno domiciliere</button>
        </div>
      </div>

    </div>





  </body>
</html>
