-- Nome DB: euroclub

CREATE TABLE artisti (
     codFiscale char(16) NOT NULL PRIMARY KEY,
     nome varchar(20) NOT NULL,
     cognome varchar(20) NOT NULL,
	 dataNascita date NOT NULL,
     telefono bigint(10) NOT NULL,
     via varchar(30) NOT NULL,
     civico int(4) NOT NULL,
     citta varchar(30) NOT NULL,
     genere varchar(20) NOT NULL,
     cachet int(4) NOT NULL,
     dj char(16),
     band char(16)
);

CREATE TABLE band (
     codFiscale char(16) NOT NULL PRIMARY KEY,
     nomeBand varchar(20) NOT NULL,
     numeroMembri int(2) NOT NULL,
	 FOREIGN KEY (codFiscale) REFERENCES artisti(codFiscale)
);

CREATE TABLE dj (
     codFiscale char(16) NOT NULL PRIMARY KEY,
     nomeDArte varchar(20) NOT NULL,
	 FOREIGN KEY (codFiscale) REFERENCES artisti(codFiscale)
);

CREATE TABLE bar (
     codBar int(2) NOT NULL PRIMARY KEY,
     nome varchar(20) NOT NULL
);

CREATE TABLE baristi (
     codFiscale char(16) NOT NULL PRIMARY KEY,
     compensoOrario int(2) NOT NULL,
     nome varchar(20) NOT NULL,
     cognome varchar(20) NOT NULL,
	 dataNascita date NOT NULL,
     telefono bigint(10) NOT NULL,
     via varchar(30) NOT NULL,
     civico int(4) NOT NULL,
     citta varchar(30) NOT NULL
);

CREATE TABLE categorie (
     codCategoria int(2) NOT NULL PRIMARY KEY,
     nome varchar(30) NOT NULL
);

CREATE TABLE clienti (
     codFiscale char(16) NOT NULL PRIMARY KEY,
     nome varchar(20) NOT NULL,
     cognome char(20) NOT NULL,
	 dataNascita date NOT NULL,
     telefono bigint(10) NOT NULL,
     via varchar(30) NOT NULL,
     civico int(4) NOT NULL,
     citta varchar(30) NOT NULL
);

CREATE TABLE consumazioni (
     codConsumazione int(4) NOT NULL PRIMARY KEY AUTO_INCREMENT,
     nome varchar(40) NOT NULL,
     prezzo decimal(5,2) NOT NULL
);

CREATE TABLE magazzini_Bar (
     codMagazzino int(4) NOT NULL PRIMARY KEY AUTO_INCREMENT,
     codFiscaleBarista char(16) NOT NULL,
	 dataMagazzino date NOT NULL,
	 FOREIGN KEY (codFiscaleBarista) REFERENCES baristi(codFiscale)
);

CREATE TABLE dettagli_Serate (
     data date NOT NULL PRIMARY KEY,
     tema varchar(20) NOT NULL,
     numeroPersoneMax int(4) NOT NULL,
     prezzoIngresso decimal(5,2) NOT NULL,
     orarioInizio time NOT NULL,
     orarioFine time NOT NULL,
     codFiscaleArtista char(16) NOT NULL,
     codMagazzino int(4) NOT NULL,
	 FOREIGN KEY (codFiscaleArtista) REFERENCES artisti(codFiscale),
	 FOREIGN KEY (codMagazzino) REFERENCES magazzini_Bar(codMagazzino)
);

CREATE TABLE dettagli_Prenotazioni (
     codPrenotazione int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
     dataPrenotazione date NOT NULL,
     numeroPersoneTavolo int(2) NOT NULL,
     note varchar(50),
     codFiscaleCliente char(16) NOT NULL,
     codFiscaleBarista char(16) NOT NULL,
     dataSerata date NOT NULL,
     FOREIGN KEY (codFiscaleCliente) REFERENCES clienti(codFiscale),
	 FOREIGN KEY (codFiscaleBarista) REFERENCES baristi(codFiscale),
	 FOREIGN KEY (dataSerata) REFERENCES dettagli_Serate(data)
);

CREATE TABLE prodotti (
     codProdotto int(4) NOT NULL PRIMARY KEY AUTO_INCREMENT,
     nome varchar(30) NOT NULL,
     quantita int(4) NOT NULL,
     codCategoria int(4) NOT NULL
);


CREATE TABLE prelievi (
     codProdotto int(4) NOT NULL,
     codMagazzino int(4) NOT NULL,
     quantitaPrelevata int(4) NOT NULL,
	 PRIMARY KEY(codProdotto, codMagazzino),
	 FOREIGN KEY (codProdotto) REFERENCES prodotti(codProdotto),
	 FOREIGN KEY (codMagazzino) REFERENCES magazzini_Bar(codMagazzino)
);

CREATE TABLE turni (
     codFiscaleBarista char(16) NOT NULL,
     data date NOT NULL,
     codBar int(2) NOT NULL,
     orarioInizio time NOT NULL,
     orarioFine time NOT NULL,
	 PRIMARY KEY(codFiscaleBarista, data),
	 FOREIGN KEY (codFiscaleBarista) REFERENCES baristi(codFiscale),     
	 UNIQUE (codBar, data)
);

CREATE TABLE vendite (
     codVendita int(4) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	 codConsumazione int(4) NOT NULL,
     quantitaVenduta int(4) NOT NULL,
     dataSerata date NOT NULL,
	 FOREIGN KEY (dataSerata) REFERENCES dettagli_Serate(data),
	 FOREIGN KEY (codConsumazione) REFERENCES consumazioni(codConsumazione)
);

INSERT INTO `clienti` (`codFiscale`, `nome`, `cognome`, `dataNascita`, `telefono`, `via`, `civico`, `citta`) VALUES
('DLVLRD98R02C573R', 'Leonardo', 'Delvecchio', '1998-10-02', 3396940884, 'sogliano', 65, 'savignano'),
('VNCLRD98T16A944N', 'Leonardo', 'Vincenzi', '1998-12-16', 3885723722, 'san bartolo', 1, 'santarcangelo'),
('MGNLRD98R08H294W', 'Leonardo', 'Magnani', '1998-10-08', 3475698742, 'sogliano', 56, 'savignano'),
('SCRLRT98M06C573V', 'Alberto', 'scarpellini', '1998-08-06', 3721596845, 'pulida', 20, 'savignano'),
('RGHLRT98E15H294V', 'Alberto', 'Righini', '1998-05-15', 3692581596, 'chiesa', 72, 'savignano'); 

INSERT INTO `baristi` (`codFiscale`, `nome`, `cognome`, `dataNascita`, `telefono`, `via`, `civico`, `citta`, `compensoOrario`) VALUES
('GROLNZ96H10C573S', 'Lorenzo', 'Gori', '1996-06-10', 3651263548, 'alfieri', 43, 'savignano', 10),
('CCCDRD98T28C573P', 'Edoardo', 'Ceccarelli', '1998-12-28', 3953871596, 'puccini', 89, 'savignano', 8),
('BGLMNL98R13S942L', 'Manuel', 'Bugli', '1998-05-10', 3334568794, 'costa', 15, 'santarcangelo', 7),
('FBBSML98E24C573J', 'Samuele', 'Fabbri', '1998-05-24', 3552593578, 'kennedy', 2, 'santarcangelo', 9),
('TMMFNC96A05C573L', 'Francesco', 'Tommasoni', '1996-01-05', 3451657895, 'borghesi', 8, 'savignano', 8);

INSERT INTO `artisti` (`codFiscale`, `nome`, `cognome`, `dataNascita`, `telefono`, `via`, `civico`, `citta`, `genere`, `cachet`, `dj`, `band`) VALUES
('PZZLCN70C08C573U', 'Luciano', 'Pazzaglia', '1970-03-08', 3541256845, 'guberti', 45, 'gambettola', 'dance anni 70/80', 300, 'PZZLCN70C08C573U', NULL),
('RSSLCU90D10H294E', 'Luca ', 'Rossi', '1990-04-10', 3452656842, 'dellargilla', 10, 'san mauro pascoli', 'techno', 250, 'RSSLCU90D10H294E', NULL),
('CMNDND60H06A944O', 'Edmondo', 'Comandini', '1960-06-06', 3481593576, 'giolitti', 56, 'borghi', 'rock', 400, NULL, 'CMNDND60H06A944O'),
('NRCBCC82B03F205X', 'Enrico', 'Bucci', '1982-02-03', 3665483652, 'panerai', 98, 'cesena', 'jazz', 450, NULL, 'NRCBCC82B03F205X'),
('ZMGGLI87M02C573Q', 'Giulio', 'Zamagni', '1987-08-02', 3445698265, 'verdi', 456, 'longiano', 'house', 200, 'ZMGGLI87M02C573Q', NULL);

INSERT INTO `band` (`codFiscale`, `nomeBand`, `numeroMembri`) VALUES
('CMNDND60H06A944O', 'COMANDINI BOYS', 4),
('NRCBCC82B03F205X', 'BUCCI BAND', 3);

INSERT INTO `dj` (`codFiscale`, `nomeDArte`) VALUES
('PZZLCN70C08C573U', 'DJ IL PAZZO'),
('RSSLCU90D10H294E', 'DJ REDS'),
('ZMGGLI87M02C573Q', 'DJ ZAMA');

INSERT INTO `turni` (`data`, `orarioInizio`, `orarioFine`, `codFiscaleBarista`, `codBar`) VALUES
('2020-09-01', '20:00:00', '23:59:00', 'GROLNZ96H10C573S', 1),
('2020-09-01', '20:00:00', '23:59:00', 'CCCDRD98T28C573P', 2),
('2020-09-02', '20:00:00', '23:59:00', 'BGLMNL98R13S942L', 2),
('2020-09-02', '20:00:00', '23:59:00', 'FBBSML98E24C573J', 1),
('2020-09-03', '18:00:00', '23:30:00', 'BGLMNL98R13S942L', 2),
('2020-09-03', '18:00:00', '23:30:00', 'TMMFNC96A05C573L', 1),
('2020-09-04', '20:00:00', '23:59:00', 'GROLNZ96H10C573S', 2),
('2020-09-04', '20:00:00', '23:59:00', 'FBBSML98E24C573J', 1),
('2020-09-08', '20:00:00', '23:59:00', 'CCCDRD98T28C573P', 2),
('2020-09-08', '20:00:00', '23:59:00', 'BGLMNL98R13S942L', 1);

INSERT INTO `bar` (`codBar`, `nome`) VALUES
(1, 'bar centrale'),
(2, 'bar piccolo');

INSERT INTO `magazzini_Bar` (`codMagazzino`, `dataMagazzino`, `codFiscaleBarista`) VALUES
(1, '2020-09-01', 'GROLNZ96H10C573S'),
(2, '2020-09-02', 'BGLMNL98R13S942L'), 
(3, '2020-09-03', 'TMMFNC96A05C573L'),
(4, '2020-09-04', 'FBBSML98E24C573J'),
(5, '2020-09-08', 'BGLMNL98R13S942L');

INSERT INTO `consumazioni` (`codConsumazione`, `nome`, `prezzo`) VALUES
(1, 'gin gordon lemon/tonic', '5.00'),
(2, 'gin tanqueray lemon/tonic', '6.00'),
(3, 'gin bombay lemon/tonic', '7.00'),
(4, 'gin mare lemon/tonic', '8.00'),
(5, 'vodka smirnoff lemon/tonic', '5.00'),
(6, 'vodka keglevich lemon/tonic', '6.00'),
(7, 'vodka sky lemon/tonic', '7.00'),
(9, 'vodka belvedere lemon/tonic', '8.00'),
(8, 'rhum pampero cocacola', '5.00'),
(10, 'rhum havana cocacola', '7.00'),
(11, 'rhum zacapa cocacola', '8.00'),
(12, 'short vodka/rhum + succo', '3.50'),
(13, 'bottiglia champagne maschio', '45.00'),
(14, 'bottiglia champagne moet', '100.00'),
(15, 'bottiglia champagne ca del bosco', '150.00'),
(16, 'amaro', '3.00'),
(17, 'birra corona', '4.00'),
(18, 'birra becks', '5.00'),
(19, 'birra ceres', '5.00'),
(20, 'birra ichnusa', '5.50'),
(21, 'acqua naturale/frizzante', '2.00');

INSERT INTO `dettagli_Serate` (`data`, `tema`, `numeroPersoneMax`, `prezzoIngresso`, `orarioInizio`, `orarioFine`, `codFiscaleArtista`, `codMagazzino`) VALUES
('2020-09-01', 'anni 80', 350, '15.00', '20:30:00', '23:59:00', 'PZZLCN70C08C573U', 1),
('2020-09-02', 'house', 450, '10.00', '20:00:00', '23:59:00', 'ZMGGLI87M02C573Q', 2),
('2020-09-03', 'jazz', 250, '25.00', '18:00:00', '23:30:00', 'NRCBCC82B03F205X', 3),
('2020-09-04', 'carnevale', 300, '16.00', '19:30:00', '23:30:00', 'RSSLCU90D10H294E', 4),
('2020-09-08', 'halloween', 300, '16.00', '20:00:00', '23:59:00', 'CMNDND60H06A944O', 5);

INSERT INTO `dettagli_Prenotazioni` (`codPrenotazione`, `dataPrenotazione`, `numeroPersoneTavolo`, `note`, `codFiscaleCliente`, `codFiscaleBarista`, `dataSerata`) VALUES
(1, '2020-08-24', 5, NULL, 'DLVLRD98R02C573R', 'GROLNZ96H10C573S', '2020-09-01'),
(2, '2020-08-25', 7, 'Tavolo vicino pista da ballo', 'VNCLRD98T16A944N', 'CCCDRD98T28C573P', '2020-09-02'),
(3, '2020-08-25', 8, NULL, 'MGNLRD98R08H294W', 'BGLMNL98R13S942L', '2020-09-01'),
(4, '2020-08-25', 6, 'Tavolo vicino Bar centrale', 'SCRLRT98M06C573V', 'TMMFNC96A05C573L', '2020-09-03'),
(5, '2020-08-26', 7, NULL, 'RGHLRT98E15H294V', 'FBBSML98E24C573J', '2020-09-01'),
(6, '2020-08-26', 8, 'Tavolo vicino pista da ballo', 'DLVLRD98R02C573R', 'TMMFNC96A05C573L', '2020-09-03'),
(7, '2020-08-27', 9, NULL, 'DLVLRD98R02C573R', 'GROLNZ96H10C573S', '2020-09-04'),
(8, '2020-08-30', 6, NULL, 'MGNLRD98R08H294W', 'CCCDRD98T28C573P', '2020-09-08');

INSERT INTO `categorie` (`codCategoria`, `nome`) VALUES
(1, 'amaro'),
(2, 'superalcolico'),
(3, 'birra'),
(4, 'bibita gassata'),
(5, 'acqua'),
(6, 'champagne'),
(7, 'succo');

INSERT INTO `vendite` (`codVendita`, `codConsumazione`, `quantitaVenduta`, `dataSerata`) VALUES
(1, 1, 20, '2020-09-01'),
(2, 2, 7, '2020-09-01'),
(3, 3, 8, '2020-09-01'),
(4, 4, 12, '2020-09-01'),
(5, 5, 20, '2020-09-01'),
(6, 6, 25, '2020-09-01'),
(7, 7, 5, '2020-09-01'),
(8, 8, 8, '2020-09-01'),
(9, 9, 12, '2020-09-01'),
(10, 10, 16, '2020-09-01'),
(11, 11, 40, '2020-09-01'),
(12, 12, 26, '2020-09-01'),
(13, 13, 13, '2020-09-01'),
(14, 14, 14, '2020-09-01'),
(15, 15, 18, '2020-09-01'),
(16, 16, 20, '2020-09-01'),
(17, 17, 32, '2020-09-01'),
(18, 18, 12, '2020-09-01'),
(19, 19, 14, '2020-09-01'),
(20, 20, 18, '2020-09-01'),
(21, 21, 25, '2020-09-01');

INSERT INTO `prodotti` (`codProdotto`, `nome`, `quantita`, `codCategoria`) VALUES
(1, 'gin gordon', 80, 2),
(2, 'vodka smirnoff', 80, 2),
(3, 'rhum pampero', 70, 2),
(4, 'coca cola', 150, 4),
(5, 'acqua tonica', 130, 4),
(6, 'succo alla pera', 100, 7),
(7, 'moet champagne', 60, 6),
(8, 'ca del bosco', 45, 6),
(9, 'maschio champagne', 80, 6),
(10, 'amaro del capo', 35, 1),
(11, 'fernet branca', 35, 1),
(12, 'amaro montenegro', 50, 1),
(13, 'gin tanqueray', 50, 2),
(14, 'gin mare', 40, 2),
(15, 'gin bombay', 40, 2),
(16, 'vodka keglevich', 60, 2),
(17, 'vodka belvedere', 30, 2),
(18, 'vodka sky', 45, 2),
(19, 'rhum havana', 40, 2),
(20, 'rhum zacapa', 15, 2),
(21, 'birra ceres', 150, 3),
(22, 'birra corona', 150, 3),
(23, 'birra ichnusa', 150, 3),
(24, 'birra becks', 150, 3),
(25, 'acqua naturale', 300, 5),
(26, 'acqua frizzante', 300, 5),
(27, 'schweppes lemon', 130, 4);

INSERT INTO `prelievi` (`codProdotto`, `codMagazzino`, `quantitaPrelevata`) VALUES
(1, 1, 4),
(2, 1, 4),
(3, 1, 4),
(4, 1, 4),
(5, 1, 15),
(6, 1, 5),
(7, 1, 4),
(8, 1, 4),
(9, 1, 4),
(10, 1, 2),
(11, 1, 2),
(12, 1, 2),
(13, 1, 4),
(14, 1, 4),
(15, 1, 4),
(16, 1, 4),
(17, 1, 4),
(18, 1, 4),
(19, 1, 4),
(20, 1, 4),
(21, 1, 15),
(22, 1, 15),
(23, 1, 15),
(24, 1, 15),
(25, 1, 30),
(26, 1, 30),
(27, 1, 15),
(1, 2, 6),
(2, 2, 6),
(3, 2, 6),
(4, 2, 6),
(5, 2, 17),
(6, 2, 7),
(7, 2, 6),
(8, 2, 6),
(9, 2, 6),
(10, 2, 3),
(11, 2, 3),
(12, 2, 3),
(13, 2, 6),
(14, 2, 6),
(15, 2, 6),
(16, 2, 6),
(17, 2, 6),
(18, 2, 6),
(19, 2, 6),
(20, 2, 6),
(21, 2, 20),
(22, 2, 20),
(23, 2, 20),
(24, 2, 20),
(25, 2, 40),
(26, 2, 40),
(27, 2, 17),
(1, 3, 3),
(2, 3, 3),
(3, 3, 3),
(4, 3, 3),
(5, 3, 3),
(6, 3, 3),
(7, 3, 3),
(8, 3, 3),
(9, 3, 3),
(10, 3, 2),
(11, 3, 2),
(12, 3, 2),
(13, 3, 4),
(14, 3, 4),
(15, 3, 4),
(16, 3, 4),
(17, 3, 4),
(18, 3, 4),
(19, 3, 4),
(20, 3, 4),
(21, 3, 18),
(22, 3, 18),
(23, 3, 18),
(24, 3, 18),
(25, 3, 30),
(26, 3, 30),
(27, 3, 15),
(1, 4, 7),
(2, 4, 7),
(3, 4, 7),
(4, 4, 7),
(5, 4, 18),
(6, 4, 8),
(7, 4, 7),
(8, 4, 7),
(9, 4, 7),
(10, 4, 3),
(11, 4, 3),
(12, 4, 3),
(13, 4, 7),
(14, 4, 7),
(15, 4, 6),
(16, 4, 6),
(17, 4, 6),
(18, 4, 6),
(19, 4, 6),
(20, 4, 6),
(21, 4, 22),
(22, 4, 22),
(23, 4, 22),
(24, 4, 22),
(25, 4, 40),
(26, 4, 40),
(27, 4, 20),
(1, 5, 7),
(2, 5, 7),
(3, 5, 7),
(4, 5, 7),
(5, 5, 18),
(6, 5, 8),
(7, 5, 7),
(8, 5, 7),
(9, 5, 7),
(10, 5, 3),
(11, 5, 3),
(12, 5, 3),
(13, 5, 7),
(14, 5, 7),
(15, 5, 6),
(16, 5, 6),
(17, 5, 6),
(18, 5, 6),
(19, 5, 6),
(20, 5, 6),
(21, 5, 22),
(22, 5, 22),
(23, 5, 22),
(24, 5, 22),
(25, 5, 40),
(26, 5, 40),
(27, 5, 20);

ALTER TABLE artisti ADD CONSTRAINT ISAartisti
     CHECK((dj IS NOT NULL AND band IS NULL)
           OR (dj IS NULL AND band IS NOT NULL)); 

