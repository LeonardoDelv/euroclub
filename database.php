<?php
class DatabaseHelper{
    private $db;


    public function __construct($servername, $username, $password, $dbname) {
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
    }

   /* public function getSedi(){      //ok
        $stmt = $this->db->prepare("SELECT idSede, nome FROM sedi");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    /*public function getOperatori(){      //ok
        $stmt = $this->db->prepare("SELECT codiceFiscale, nome, cognome FROM dipendenti WHERE ruolo = 'operatore'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    public function getBaristi() {      //ok
      $stmt = $this->db->prepare("SELECT codFiscale, nome, cognome FROM baristi");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getArtisti() {      //ok
      $stmt = $this->db->prepare("SELECT codiceFiscale, nome, cognome FROM artisti");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getClienti() {      //ok
      $stmt = $this->db->prepare("SELECT codFiscale, nome, cognome FROM clienti");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*public function getDomicilieri() {    //ok
        $stmt = $this->db->prepare("SELECT codiceFiscale, nome, cognome FROM dipendenti WHERE ruolo = 'domiciliere'");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    /*public function getCampanelli() {
        $stmt = $this->db->prepare("SELECT * FROM clienti");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    public function getProdotti() {
      $stmt = $this->db->prepare("SELECT * FROM prodotti");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getConsumazioni() {
      $stmt = $this->db->prepare("SELECT * FROM consumazioni");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }


    /*public function insert_dipendente() {     //ok
      $stmt = $this->db->prepare('INSERT INTO dipendenti (codiceFiscale, ruolo, nome, cognome, compensoOrario, numeroTelefono, dataNascita,
      via, civico, citta) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
      //$data = $_POST['birthdate'];
      // $data = date("Y-m-d");
      $stmt->bind_param('ssssiissis', $_POST['CF'], $_POST['ruolo'], $_POST['nome'], $_POST['cognome'], $_POST['compenso'],  $_POST['tel'],
                          $_POST['birthdate'] , $_POST['via'], $_POST['civico'], $_POST['città']);
      $stmt->execute();
    }*/

    public function insert_barista() {     //ok
      $stmt = $this->db->prepare('INSERT INTO dipendenti (codiceFiscale, compensoOrario, nome, cognome, dataNascita, telefono, dataNascita,
      via, civico, citta) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
      //$data = $_POST['birthdate'];
      // $data = date("Y-m-d");
      $stmt->bind_param('ssssiissis', $_POST['CF'], $_POST['compenso'], $_POST['nome'], $_POST['cognome'],  $_POST['tel'],
                          $_POST['birthdate'] , $_POST['via'], $_POST['civico'], $_POST['citta']);
      $stmt->execute();
    }

    /*public function insertTurnoOperatore(){
      $stmt = $this->db->prepare('INSERT INTO turni (data, orarioInizio, orarioFine, CF_Operatore)
      VALUES (?,?,?,?)');
      $stmt->bind_param('ssss', $_POST['date'], $_POST['oraI'], $_POST['oraF'], $_POST['operatore']);
      $stmt->execute();
    }*/

    public function insertTurnoBarista() {
      $stmt = $this->db->prepare('INSERT INTO turni (codFiscaleBarista, data, orarioInizio, orarioFine, )
      VALUES (?,?,?,?)');
      $stmt->bind_param('ssss', $_POST['barista'], $_POST['date'], $_POST['oraI'], $_POST['oraF']);
      $stmt->execute();
    }

    /*public function insertTurnoDomiciliere(){
      $stmt = $this->db->prepare('INSERT INTO turni_domicilieri (data, CF_Operatore, CF_Domiciliere)
      VALUES (?,?,?)');
      var_dump($_POST['data']);
      var_dump($_POST['operatore']);
      var_dump($_POST['domic']);
      $stmt->bind_param('sss', $_POST['data'], $_POST['operatore'], $_POST['domic']);
      $stmt->execute();
    }*/

    /*public function insertOrdine() {
      $stmt = $this->db->prepare('INSERT INTO ordini (numOrdine, data, importoTotale, orario, domicilio, prezzoConsegna, CF_Operatore, CF_Domiciliere, idCliente)
      VALUES (?,?,?,?,?,?,?,?,?)');

      if($_POST['tipo'] == 'Domicilio'){
        $dom = 1;
      } else {
        $dom = 0;
      }

      $zero = 0;
      $ord = 1;

      $stmt->bind_param('isisiissi', $ord , $_POST['date'], $zero , $_POST['ora'], $dom , $_POST['prezzocon'], $_POST['operatore'], $_POST['domiciliere'] , $_POST['campanello']);
      $stmt->execute();

    }*/

    /*public function getLastOrder(){
      $stmt = $this->db->prepare('SELECT MAX(idOrdine) AS lastOrd FROM ordini');
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    public function getLastPrenotazione() {
      $stmt = $this->db->prepare('SELECT MAX(codPrenotazione) AS lastPrenotaz FROM dettaglio_Prenotazioni');
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*public function insertDettaglioOrdine($id){
      $stmt = $this->db->prepare('INSERT INTO dettaglio_ordini (idOrdine, idProdotto, idVariazione, quantita, prezzo)
                                        VALUES (?,?,?,?,?)');
      $uno = 1;
      //$id = $_POST['last'] + 1;
      //var_dump($id);
      $stmt->bind_param('iiiii', $id, $_POST['prodotto'], $_POST['variazione'], $uno, $uno);
      $stmt->execute();
    }*/

    public function insertDettaglioPrenotazione($id) {
      $stmt = $this->db->prepare('INSERT INTO dettagli_Prenotazioni (codPrenotazione, dataPrenotazione, numeroPersoneTavolo, note, codFiscaleCliente, codFiscaleBarista, dataSerata)
                                        VALUES (?,?,?,?,?)');
      $uno = 1;
      //$id = $_POST['last'] + 1;
      //var_dump($id);
      $stmt->bind_param('iiiii', $id, $_POST['dataPrenotazione'], $_POST['numeroPersoneTavolo'], $_POST['note'], $_POST['codFiscaleCliente'], $_POST['codFiscaleBarista'], $_POST['dataSerata']);
      $stmt->execute();

    }

    // Da svolgere POI

    /*public function calcImportoOrdine($id) {
      $stmt = $this->db->prepare('SELECT dor.idOrdine, SUM(v.prezzo + p.prezzoUnitario) AS "PrezzoTot"
      FROM variazioni v, prodotti p, dettaglio_ordini dor
      WHERE v.idVariazione = dor.idVariazione
      AND p.idProdotto = dor.idProdotto
      AND dor.idOrdine = ?');

      //$id = $_POST['last'] + 1;
      var_dump($id);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function updateTotOrdine($tot,$id){
      $stmt = $this->db->prepare("UPDATE ordini SET ordini.importoTotale=(ordini.prezzoConsegna + ?)
      WHERE idOrdine=?");
      var_dump($tot);
      $stmt->bind_param('si', $tot, $id);
      $stmt->execute();
      $stmt->store_result();
    }*/

    public function insertClient($codFiscale, $nome, $cognome, $dataNascita, $tel, $via, $civico, $citta) {
      $stmt = $this->db->prepare('INSERT INTO clienti (codFiscale, nome, cognome, telefono, via, civico, citta) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
      $stmt->bind_param('ssississ', $codFiscale, $nome, $cognome, $dataNascita, $tel, $via, $civico, $citta);
      $stmt->execute();
      $stmt->store_result();
    }

    /*public function getTurniOperatori() {   //ok
      $stmt = $this->db->prepare('SELECT t.CF_Operatore, d.nome, d.cognome, t.data, t.orarioInizio, t.orarioFine
      FROM turni t, dipendenti d
      WHERE t.CF_operatore = d.codiceFiscale
      ');
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    public function getTurniBaristi() {   //ok
      $stmt = $this->db->prepare('SELECT t.codFiscaleBarista, b.nome, b.cognome, t.data, t.codBar, t.orarioInizio, t.orarioFine
      FROM turni t, baristi b, bb bar
      WHERE t.codFiscaleBarista = b.codiceFiscale
      AND bb.codBar = t.codBar');
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*public function getTOperatori($cf){
      $stmt = $this->db->prepare('SELECT t.CF_Operatore, d.nome, d.cognome, t.data, t.orarioInizio, t.orarioFine
      FROM turni t, dipendenti d
      WHERE t.CF_operatore = d.codiceFiscale
      AND t.CF_operatore = ?
      ');
      $stmt->bind_param('s',$cf);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }*/

    public function getTBaristi($cf) {
      $stmt = $this->db->prepare('SELECT t.codFiscaleBarista, b.nome, b.cognome, t.data, t.codBar, t.orarioInizio, t.orarioFine
      FROM turni t, baristi b
      WHERE t.codFiscaleBarista = b.codiceFiscale
      AND t.codFiscaleBarista = ?
      AND bb.codBar = t.codBar');
      $stmt->bind_param('s',$cf);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }


    /*public function getCompensoOreDomiciliere($domiciliere){
      $stmt = $this->db->prepare('SELECT d.nome, d.cognome, SUM(TIMESTAMPDIFF(HOUR,t.orarioInizio, t.orarioFine)) AS "oreLavorate",  SUM(TIMESTAMPDIFF(HOUR, t.orarioInizio, t.orarioFine)*d.compensoOrario) AS "compensoMensile"
      FROM turni t, turni_domicilieri td, dipendenti d
      WHERE td.data = t.data
      AND td.CF_Domiciliere = d.codiceFiscale
      AND td.CF_Domiciliere = ?
      AND MONTH(td.data) = ?
      ');
      $month = date('m');
     // $domiciliere = 'CLLNDR00A23O952E';
      $stmt->bind_param('ss',$domiciliere, $month);
      $stmt->execute();
      $result = $stmt->get_result();
      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTurniDomicilieri(){
      $stmt = $this->db->prepare('SELECT td.CF_Domiciliere, d.nome, d.cognome, t.data, t.orarioInizio, t.orarioFine
      FROM turni t, turni_domicilieri td, dipendenti d
      WHERE t.data = td.data
      AND d.codiceFiscale = td.CF_Domiciliere
      AND d.ruolo = ?
      ');
      $domiciliere = 'domiciliere';
      $stmt->bind_param('s', $domiciliere);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getTotDomicili($data){
      $stmt = $this->db->prepare('SELECT COUNT(o.idOrdine), SUM(importoTotale)
      FROM ordini o
      WHERE o.data= ?
      ');
      $data = date("Y-m-d");
      $stmt->bind_param('s', $data);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }*/

}

?>
