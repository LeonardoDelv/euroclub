<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Cliente</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php';?>
    <?php sec_session_start();
    if(!empty($_POST['cliente'])){
      $_SESSION['cliente'] = $_POST['cliente'];
    }
    ?>
    <?php require_once 'navbar_home.php';
    ?>

    <div class="container justify-content-center col-md-9">
      <hr>
        <h3 class="text-center">Home Cliente</h3>
      <hr>
      <h5 class="text-center">Visualizza prossime serate</h5>
      <br>
      <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal1">Visualizza Prossime Serate</button>
      </div>
      <!--
      <div class="form-group">
        <form id="iT" action="insert_cliente.php" method="post">
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Cliente</button>
        </div>
      </div>-->

      <hr>
      <h5 class="text-center">Visualizza serate artista</h5>
      <br>
      <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal2">Visualizza Serate Artista</button>
        </div>
      <!--
      <div class="form-group">
        <form id="iT" action="insert_dettagliPrenotazioni.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php/* echo $_POST['operatore']; */?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Prenotazione</button>
        </div>
      </div>
        -->

      <hr>
      <h5 class="text-center">Visualizza lista cocktails</h5>
      <br>
      <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal3">Visualizza Lista Cocktails</button>
        </div>
      <!--
      <div class="form-group">
        <form id="iT" action="insert_turno_op.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php/* echo $_POST['operatore'];*/ ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci turno operatore</button>
        </div>
      </div>
        -->
      <!--
      <hr>
      <h5 class="text-center">Inserisci turni domiciliere</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_turno_dom.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php /* echo $_POST['operatore']; */ ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci turno domiciliere</button>
        </div>
      </div> -->
    <!--
      <hr>
      <h5 class="text-center">Inserisci nuova consumazione</h5>
      <br>
      <div class="form-group">
        <form id="iT" action="insert_turno_dom.php" method="post">
        <input type="hidden" class="form-control" name="cfoperatore" id="cfoperatore" value="<?php /* echo $_POST['operatore']; */ ?>" >
        <div class="text-center">
        </form>
        <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci nuova consumazione</button>
        </div>
      </div>
      -->
    </div>
  </body>
</html>
