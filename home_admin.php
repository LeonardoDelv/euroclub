<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Homepage - Admin</title>
    <!-- <link href="style.css" rel="stylesheet"> -->
  </head>
  <body>
    <?php require_once 'functions.php';
    require_once 'bootstrap.php';?>
    <?php sec_session_start();
    //if(empty($_SESSION['admin'])){
    if(!empty($_POST['admins'])){
      $_SESSION['admin'] = $_POST['admins'];
    }
    //} else {}
    //var_dump($_SESSION['admin']); ?>
    <?php require_once 'navbar_home.php';
    //sec_session_start();
    //$turni_o = $dbh->getTurniOperatori($_SESSION['admin']);
    //$turni_d = $dbh->getTurniDomicilieri($_SESSION['admin']);
    //$numDomicili = $dbh->getTotDOmicili($_SESSION['admin']);

    require_once 'modals.php';

    ?>

    <div class="container justify-content-center col-md-9">
      <hr>
      <h3 class="text-center">Home Amministratore</h3>
      <hr>

      <h5 class="text-center">Inserisci nuova serata</h5>
      <br>
      <div class="form-group">
          <form id="iP" action="insert_serata.php" method="post">
          <div class="text-center">
          </form>
          <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Nuova Serata</button>
          </div>
      </div>
      <hr>

      <h5 class="text-center">Inserisci un nuovo barista</h5>
      <br>
      <div class="form-group">
          <form id="iP" action="insert_barista.php" method="post">
          <div class="text-center">
          </form>
          <button id="btn-info" type="submit" class="btn btn-outline-primary">Inserisci Barista</button>
          </div>
      </div>
      <hr>

      <h5 class="text-center">Visualizza turni lavorativi dei baristi</h5>
      <br>
        <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal2">Visualizza Turni Baristi</button>
        </div>
      
      <hr>
      <h5 class="text-center">Visualizza ore lavorate e compensi</h5>
      <br>
        <div class="text-center">
          <button id="btn-info" type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#modal1">Visualizza ore lavorate e compensi</button>
        </div>
    </div>
  </body>
</html>
