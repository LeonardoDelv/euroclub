<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Uniburger - Inserisci personale</title>

  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';

    ?>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_admin.php">Home Amministratore</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Dipendente</li>
        </ol>
    </nav>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento Dipendente </h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-registrazione" action="insert_barista_function.php" method="post">
      <!--<label for="ruolo">Ruolo</label>
      <select class="form-control" name="ruolo" style="display: inline-block; margin-top: 2%;">
          <option value="operatore">Operatore</option>
          <option value="domiciliere">Domiciliere</option>
      </select>
      <br>-->
      <label for="nome">Nome</label>
      <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" maxlength="30" required>

      <label for="cognome">Cognome</label>
      <input type="text" class="form-control" name="cognome" id="cognome" placeholder="Cognome" maxlength="30" required>

      <label for="CF">Codice Fiscale</label>
      <input type="text" class="form-control" name="CF" id="CF" placeholder="Codice Fiscale" maxlength="16" required>

      <label for="tel">Telefono</label>
      <input type="tel" class="form-control" name="tel" id="tel" placeholder="Telefono" maxlength="10" required>

      <label for="date">Data di nascita</label>
      <input type="date" class="form-control" name="birthdate" id="birthdate" placeholder="Data di nascita" max="2001-08-25" required>

      <label for="compenso">Compenso Orario</label>
      <input type="text" class="form-control" name="compenso" id="compenso" placeholder="Compenso Orario" maxlength="3" required>

      <label for="via">Via</label>
      <input type="text" class="form-control" name="via" id="via" placeholder="Via" required>

      <label for="civico">Civico</label>
      <input type="text" class="form-control" name="civico" id="civico" placeholder="Civico" required>

      <label for="città">Città</label>
      <input type="text" class="form-control" name="città" id="città" placeholder="Città" required>
      <br>
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
