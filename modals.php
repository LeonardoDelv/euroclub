<!-- <button id="btn-info" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal1">Dettagli Prelievi</button>
<button id="btn-info" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal2">Visualizza Turni</button> -->

<div id="modal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalA" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione ore lavorate e compensi baristi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="nome" width="10%" scope="col">NOME</th>
                          <th id="cognome" width="10%" scope="col">COGNOME</th>
                          <th id="tipo" width="10%" scope="col">ORE</th>
                          <th id="qtà" width="5%" scope="col">COMPENSO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php/*
                            $domicilieri = $dbh->getDomicilieri();

                            foreach($domicilieri as $dom):
                            $compensoOre = $dbh->getCompensoOreDomiciliere($dom['codiceFiscale']);
                            foreach ($compensoOre as $co): */?>
                            <td><?php/* echo($co['nome']);*/ ?></td>
                            <td><?php/* echo($co['cognome']);*/ ?></td>
                            <td><?php/* echo($co['oreLavorate']); */?></td>
                            <td><?php/* echo($co['compensoMensile'])*/ ?></td>
                        </tr>
                          <?php/* endforeach;
                          endforeach;*/?>

                      </tbody>
                  </table>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>


<div id="modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalB" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione turni Baristi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="data>">DATA</th>
                          <th>NOME</th>
                          <th>COGNOME</th>
                          <th>ORA INIZIO</th>
                          <th>ORA FINE</th>
                          <th>BAR</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php/* foreach ($turni_o as $to):*/ ?>
                            <td headers="data"><?php/* echo($to['data']);*/ ?></td>
                            <td><?php/* echo($to['nome']); */?></td>
                            <td><?php/* echo($to['cognome']); */?></td>
                            <td><?php/* echo($to['orarioInizio']) */?></td>
                            <td><?php/* echo($to['orarioFine']) */?></td>
                        </tr>
                          <?php/* endforeach; */?>
                      </tbody>
                  </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<!--<div id="modal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalB" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalA">Visualizzazione turni Domicilieri</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


              <div class="table-responsive-xl">
                  <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th id="data>">DATA</th>
                          <th>NOME</th>
                          <th>COGNOME</th>
                          <th>ORA INIZIO</th>
                          <th>ORA FINE</th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php/* foreach ($turni_d as $ti):*/ ?>
                            <td headers="data"><?php/* echo($ti['data']); */?></td>
                            <td><?php/* echo($ti['nome']); */?></td>
                            <td><?php/* echo($ti['cognome']);*/ ?></td>
                            <td><?php/* echo($ti['orarioInizio']) */?></td>
                            <td><?php/* echo($ti['orarioFine'])*/ ?></td>
                        </tr>
                          <?php /*endforeach; */?>
                      </tbody>
                  </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>-->
