<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Uniburger - Inserisci Ordine</title>

  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';
    sec_session_start();

    $campanelli = $dbh->getCampanelli();
    $prodotti = $dbh->getProdotti();
    $variazioni = $dbh->getVariazioni();
    $domicilieri = $dbh->getDomicilieri();

    ?>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_barista.php">Home Operatore</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Ordine</li>
        </ol>
    </nav>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento ordine</h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-turno" action="insert_dettagliPrenotazioni_function.php" method="post">

      <label for="date">Data</label>
      <input type="date" class="form-control" name="date" id="date" placeholder="Data" required>

      <label for="ora">Orario</label>
      <input type="time" class="form-control" name="ora" id="ora" placeholder="Orario" required>

      <label for="tipo">Tipo</label>
      <select name="tipo" class="form-control" style="display: inline-block; margin-top: 2%;">
        <option value="Domicilio">Domicilio</option>
        <option value="Ritiro">Ritiro</option>
      </select>

      <label for="prezzocon"></label>
      <input type="text" class="form-control" name="prezzocon" id="prezzocon" placeholder="PrezzoConsegna" required>

      <label for="campanello">Campanello</label>
      <select name="campanello"  class="form-control" style="display: inline-block; margin-top: 2%;">
        <?php foreach($campanelli as $campanello): ?>
          <option value="<?php echo $campanello['idCliente']; ?>"><?php echo $campanello['campanello']; ?></option>
        <?php endforeach; ?>
      </select>

      <label for="prodotto">Prodotto</label>
      <select name="prodotto"  class="form-control" style="display: inline-block; margin-top: 2%;">
        <?php foreach($prodotti as $prodotto): ?>
          <option value="<?php echo $prodotto['idProdotto']; ?>"><?php echo $prodotto['nome']; ?></option>
        <?php endforeach; ?>
      </select>

      <label for="variazione">Variazione</label>
      <select name="variazione"  class="form-control" style="display: inline-block; margin-top: 2%;">
        <?php foreach($variazioni as $variazione): ?>
          <option value="<?php echo $variazione['idVariazione']; ?>"><?php echo $variazione['nome']; ?></option>
        <?php endforeach; ?>
      </select>

      <label for="domiciliere">Domiciliere</label>
      <select name="domiciliere"  class="form-control" style="display: inline-block; margin-top: 2%;">
          <option value=""></option>
        <?php foreach($domicilieri as $domiciliere): ?>
          <option value="<?php echo $domiciliere['codiceFiscale']; ?>"><?php echo $domiciliere['codiceFiscale']; ?></option>
        <?php endforeach; ?>
      </select>

      <input type="hidden" class="form-control" name="operatore" id="operatore" value="<?php echo $_POST['cfoperatore']; ?>" >

      <?php //foreach($last as $ultimo): ?>
      <!-- <input type="hidden" class="form-control" name="last" id="last" value="<//?php echo $ultimo['lastOrd']; ?>"> -->
      <?php //endforeach;?>
      <br>
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
