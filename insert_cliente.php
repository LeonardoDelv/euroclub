<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Uniburger - Inserisci cliente</title>

  </head>
  <body>
    <?php require_once 'functions.php'; ?>
    <?php require_once 'navbar_home.php';
    require_once 'bootstrap.php';

    ?>
    <style media="screen">
      label{
        margin-top: 2%;
        margin-bottom: 1%;
      }
    </style>
    <nav aria-label="breadcrumb" style="margin-top: 1%;">
        <ol class="breadcrumb bg-light">
            <li class="breadcrumb-item"><a href="home_barista.php">Home Barista</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inserisci Nuovo Cliente</li>
        </ol>
    </nav>
  <div class="container justify-content-center col-md-4">
    <h3 class="text-center">Inserimento cliente</h3>
  <hr class="upRegister">
  <div class="form-group">
    <form id="form-registrazione" action="insert_cliente_function.php" method="post">
      <label for="cf">Codice Fiscale</label>
      <input type="text" class="form-control" name="cf" id="cf" placeholder="Codice Fiscale" required>

      <label for="nome">Nome</label>
      <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" required>

      <label for="cognome">Cognome</label>
      <input type="text" class="form-control" name="cognome" id="cognome" placeholder="Cognome" required>

      <label for="dataNascita">Data di Nascita</label>
      <input type="date" class="form-control" name="dataNascita" id="dataNascita" placeholder="Data di nascita" required>

      <label for="tel">Telefono</label>
      <input type="tel" class="form-control" name="tel" id="tel" placeholder="Telefono" maxlength="10" required>

      <label for="via">Via</label>
      <input type="text" class="form-control" name="via" id="via" placeholder="Via" required>

      <label for="civico">Civico</label>
      <input type="text" class="form-control" name="civico" id="civico" placeholder="Civico" required>

      <label for="città">Città</label>
      <input type="text" class="form-control" name="città" id="città" placeholder="Città" required>
      
      <br>
      <button type="submit" class="btn btn-primary" style="display: block;">Conferma</button>
    </form>
  </div>
  </div>

</body>
</html>
